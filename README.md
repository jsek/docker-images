# TeamCity build agent configuration

Typically TeamCity agents running in containers need to reach TeamCity server and be able to run the commands you wish to execute in build steps.

To test how it works you can simply run [Minimal Build Agent](https://hub.docker.com/r/jetbrains/teamcity-minimal-agent/).

Assuming your TeamCity server is running on localhost:90 and your connection to containers is based on Virtual Switch (e.g. on Hyper-V), by default you only need to provide these 2 environment variables while running the container:

| Variable | Value |
| --- | --- |
| SERVER_URL | 10.0.75.1:90 |
| AGENT_NAME | docker-agent-1 |

Expect new Unauthorized agent to appear at [/agents.html?tab=unauthorizedAgents](localhost:90/agents.html?tab=unauthorizedAgents)

## Creating specialized image

If you managed to connect dockerized agent, you may want to create specialized image with build commands necessary to build your project.

### Make it possible for containers to reach internet

Re-configure:

    - Docker to 192.168.137.0
    - DockerNAT to 192.168.137.1

### Create DOCKERFILE (Node.js example)

Base on [specialized image for Java](https://github.com/JetBrains/teamcity-docker-agent/blob/master/Dockerfile) I swapped installation commands from this [article](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-ubuntu-16-04) with tips from [StackOverflow](https://stackoverflow.com/questions/25899912/install-nvm-in-docker) to have Node.js installed

> After couple of tries, adjusting paths, fixing version numbers...

The result is this [DOCKERFILE](teamcity-agent-nodejs/DOCKERFILE).

### Build image

```sh
docker build -t jsek/teamcity-agent-nodejs .
```

Test locally with these variables

| Variable | Value |
| --- | --- |
| SERVER_URL | 192.168.137.1:90 |
| AGENT_NAME | node-agent-1 |

```sh
docker login
docker tag jsek/teamcity-agent-nodejs jsek/teamcity-agent-nodejs:latest
docker push jsek/teamcity-agent-nodejs:latest
```